<?php

$attentie_image = get_field('attentie_afbeelding');
$attentie_titel = get_field('attentie_titel');
$attentie_button_text = get_field('attentie_button_tekst');
$attentie_button_url = get_field('attentie_button_url');
$attentie_button_url = get_field('attentie_button_url');

$attentie_button_visible = get_field('attentie_button_visible');

?>

<div class="section m-attention padding-resp-top padding-resp-bottom">

	<div class="l-container">
		<div class="l-row">
			<div class="col-xs-12">

				<div class="m-attention__image" style="background: url(<?php echo $attentie_image; ?>) no-repeat center center; background-size: cover;">
				</div>
				<div class="m-attention__content">
					<div class="m-attention__content__left bg--color-primary padding-resp">
						<h3 class="txt--white"><?php echo $attentie_titel; ?></h3>
						
						<?php if ($attentie_button_visible) { ?>
						<a href="<?php echo $attentie_button_url; ?>" class="o-btn o-btn--primary"><?php echo $attentie_button_text; ?> <i class="fa fa-angle-right"></i></a>
						<?php } ?>

					</div>
					<div class="m-attention__content__right bg--white padding-resp">
						
						<?php

							$count = 1;

							// check if the repeater field has rows of data
							if( have_rows('attentie_content_item') ):
							
							 	// loop through the rows of data
							    while ( have_rows('attentie_content_item') ) : the_row();

							        // display a sub field value
									$attentie_content_item_tekst = get_sub_field('attentie_content_item_tekst');
							        
							        ?>
										
										<div class="m-attention__content__item">
											<div class="number">
												<strong><?php echo($count); ?></strong>
											</div>
											<div class="content">
												<?php echo($attentie_content_item_tekst); ?>
											</div>
										</div>

							        <?php

							        $count++;
							        
							    endwhile;

							else :

							    // no rows found

							endif;

						?>

						
					</div>
				</div>
				
			</div>
		</div>
	</div>

</div>