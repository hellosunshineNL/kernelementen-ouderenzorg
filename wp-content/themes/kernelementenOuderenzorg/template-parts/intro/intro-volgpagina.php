<?php

$pagina_titel = get_field('pagina_titel');
$pagina_tekst = get_field('pagina_tekst');

?>

<div class="section m-intro-volgpagina bg--color-primary padding-resp-top padding-resp-bottom">
	
	<div class="l-container">
		<div class="l-row">

			<div class="col-xs-12">
				<div class="m-intro-volgpagina__content">
					<h1><?php echo $pagina_titel; ?></h1>
					<?php echo $pagina_tekst; ?>
				</div>
			</div>

		</div>
	</div>

</div>