<?php

$intro_home_image = get_field('intro_home_afbeelding');
$intro_home_tekst_bold = get_field('intro_home_tekst_bold');
$intro_home_tekst_regular = get_field('intro_home_tekst_regular');

?>

<div class="section m-intro-home">

	<div class="l-container">
		<div class="l-row">

			<div class="col-xs-12 col-sm-7 col-md-8 m-intro-home__image">
				<div class="m-intro-home__image__bg" style="background: url(<?php echo($intro_home_image); ?>) no-repeat center center; background-size: cover;"></div>
			</div>

			<div class="col-xs-12 col-sm-5 col-md-4 m-intro-home__content">
				<div class="m-intro-home__content__inner bg--color-primary padding-resp">
					<strong><?php echo($intro_home_tekst_bold); ?></strong>
					<p><?php echo($intro_home_tekst_regular); ?></p>
				</div>
			</div>

		</div>
	</div>

</div>