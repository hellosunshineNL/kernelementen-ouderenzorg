<header id="l-header">

	<div id="l-header__top">
		<div class="l-container">
				
			<?php get_template_part('template-parts/logo/logo'); ?>
			<?php get_template_part('template-parts/social-media/social-media'); ?>

		</div>
	</div>

	<div id="l-header__navigation">
		<div class="l-container">
			<div class="l-row">
				<div class="col-xs-12">

					<?php get_template_part('template-parts/navigation/navigation', 'main'); ?>

				</div>
			</div>
		</div>
	</div>

</header>