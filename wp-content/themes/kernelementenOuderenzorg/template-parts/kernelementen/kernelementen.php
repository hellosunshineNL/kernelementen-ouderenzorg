<?php

$kernelementen_titel = get_field('kernelementen_titel');
$kernelement_1 = get_field('kernelement_1');
$kernelement_2 = get_field('kernelement_2');
$kernelement_3 = get_field('kernelement_3');
$kernelement_4 = get_field('kernelement_4');
$kernelement_5 = get_field('kernelement_5');
$kernelement_6 = get_field('kernelement_6');
$kernelement_7 = get_field('kernelement_7');
$kernelement_8 = get_field('kernelement_8');

?>

<div class="section m-kernelementen bg--gray4 padding-resp-top padding-resp-bottom section-negative-top">
	
	<div class="l-container">

		<div class="l-row">
			<div class="col-xs-12">
				<div class="m-kernelementen__top-content">
					<h2 class="txt--white"><?php echo $kernelementen_titel; ?></h2>
				</div>
			</div>
		</div>

		<div class="l-row">
			
			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>1</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_1; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>2</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_2; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>3</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_3; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>4</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_4; ?>
					</div>
				</div>
			</div>

		</div>

		<div class="l-row">

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>5</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_5; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>6</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_6; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>7</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_7; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>8</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_8; ?>
					</div>
				</div>
			</div>

		</div>

		<div class="l-row">
			<div class="col-xs-12 align-right">
				<a href="http://kernelementenouderenzorg.nl/de-8-kernelementen/" class="o-btn o-btn--black">Lees meer <i class="fa fa-angle-right"></i></a>
			</div>
		</div>

	</div>
	
</div>