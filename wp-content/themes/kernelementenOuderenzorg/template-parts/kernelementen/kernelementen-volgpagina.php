<?php

$kernelementen_volgpagina_titel = get_field('kernelementen_volgpagina_titel');
$kernelement_volgpagina_1 = get_field('kernelement_volgpagina_1');
$kernelement_volgpagina_2 = get_field('kernelement_volgpagina_2');
$kernelement_volgpagina_3 = get_field('kernelement_volgpagina_3');
$kernelement_volgpagina_4 = get_field('kernelement_volgpagina_4');
$kernelement_volgpagina_5 = get_field('kernelement_volgpagina_5');
$kernelement_volgpagina_6 = get_field('kernelement_volgpagina_6');
$kernelement_volgpagina_7 = get_field('kernelement_volgpagina_7');
$kernelement_volgpagina_8 = get_field('kernelement_volgpagina_8');
$kernelement_volgpagina_button_visible = get_field('kernelement_volgpagina_button_visible');
$kernelement_volgpagina_button_tekst = get_field('kernelement_volgpagina_button_tekst');
$kernelement_volgpagina_button_url = get_field('kernelement_volgpagina_button_url');

?>

<div class="section m-kernelementen bg--gray4 padding-resp-top padding-resp-bottom--xl">
	<div class="l-container">

		<div class="l-row">
			<div class="col-xs-12">
				<div class="m-kernelementen__top-content">
					<h2 class="txt--white">Wat zijn de <strong>8 kernelementen</strong>?</h2>
				</div>
			</div>
		</div>

		<div class="l-row">
			
			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>1</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_volgpagina_1; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>2</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_volgpagina_2; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>3</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_volgpagina_3; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>4</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_volgpagina_4; ?>
					</div>
				</div>
			</div>

		</div>

		<div class="l-row">

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>5</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_volgpagina_5; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>6</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_volgpagina_6; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>7</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_volgpagina_7; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>8</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<?php echo $kernelement_volgpagina_8; ?>
					</div>
				</div>
			</div>

		</div>
		
		<?php if ($kernelement_volgpagina_button_visible) { ?>
			<div class="l-row">
				<div class="col-xs-12 align-right">
					<a href="<?php echo $kernelement_volgpagina_button_url; ?>" class="o-btn o-btn--black"><?php echo $kernelement_volgpagina_button_tekst; ?> <i class="fa fa-angle-right"></i></a>
				</div>
			</div>
		<?php } ?>

	</div>
</div>