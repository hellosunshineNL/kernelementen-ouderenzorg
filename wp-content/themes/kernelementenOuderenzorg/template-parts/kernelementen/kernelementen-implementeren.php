<?php

$kernelementen_implementeren_titel = get_field('kernelementen_implementeren_titel');
$kernelementen_implementeren_content = get_field('kernelementen_implementeren_content');
$kernelement_implementeren_bold_1 = get_field('kernelement_implementeren_bold_1');
$kernelement_implementeren_1 = get_field('kernelement_implementeren_1');
$kernelement_implementeren_bold_2 = get_field('kernelement_implementeren_bold_2');
$kernelement_implementeren_2 = get_field('kernelement_implementeren_2');
$kernelement_implementeren_bold_3 = get_field('kernelement_implementeren_bold_3');
$kernelement_implementeren_3 = get_field('kernelement_implementeren_3');
$kernelement_implementeren_bold_4 = get_field('kernelement_implementeren_bold_4');
$kernelement_implementeren_4 = get_field('kernelement_implementeren_4');
$kernelement_implementeren_bold_5 = get_field('kernelement_implementeren_bold_5');
$kernelement_implementeren_5 = get_field('kernelement_implementeren_5');
$kernelement_implementeren_bold_6 = get_field('kernelement_implementeren_bold_6');
$kernelement_implementeren_6 = get_field('kernelement_implementeren_6');
$kernelement_implementeren_bold_7 = get_field('kernelement_implementeren_bold_7');
$kernelement_implementeren_7 = get_field('kernelement_implementeren_7');
$kernelement_implementeren_bold_8 = get_field('kernelement_implementeren_bold_8');
$kernelement_implementeren_8 = get_field('kernelement_implementeren_8');


?>

<div class="section m-kernelementen bg--white padding-resp-top padding-resp-bottom">
	<div class="l-container">

		<div class="l-row">
			<div class="col-xs-12">
				<div class="m-kernelementen__top-content">
					<h2 class="txt--black"><?php echo $kernelementen_implementeren_titel; ?></h2>
					<?php echo $kernelementen_implementeren_content; ?>
				</div>
			</div>
		</div>

		<div class="l-row">
			
			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>1</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<strong><?php echo $kernelement_implementeren_bold_1; ?></strong><br>
						<?php echo $kernelement_implementeren_1; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>2</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<strong><?php echo $kernelement_implementeren_bold_2; ?></strong><br>
						<?php echo $kernelement_implementeren_2; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>3</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<strong><?php echo $kernelement_implementeren_bold_3; ?></strong><br>
						<?php echo $kernelement_implementeren_3; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>4</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<strong><?php echo $kernelement_implementeren_bold_4; ?></strong><br>
						<?php echo $kernelement_implementeren_4; ?>
					</div>
				</div>
			</div>

		</div>

		<div class="l-row">

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>5</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<strong><?php echo $kernelement_implementeren_bold_5; ?></strong><br>
						<?php echo $kernelement_implementeren_5; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>6</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<strong><?php echo $kernelement_implementeren_bold_6; ?></strong><br>
						<?php echo $kernelement_implementeren_6; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>7</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<strong><?php echo $kernelement_implementeren_bold_7; ?></strong><br>
						<?php echo $kernelement_implementeren_7; ?>
					</div>
				</div>
			</div>

			<div class="m-kernelementen__item col-xs-6 col-sm-3 col-md-3">
				<div class="m-kernelementen__item__number">
					<strong>8</strong>
				</div>
				<div class="m-kernelementen__item__text">
					<div class="m-kernelementen__item__text__inner">
						<strong><?php echo $kernelement_implementeren_bold_6; ?></strong><br>
						<?php echo $kernelement_implementeren_8; ?>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>