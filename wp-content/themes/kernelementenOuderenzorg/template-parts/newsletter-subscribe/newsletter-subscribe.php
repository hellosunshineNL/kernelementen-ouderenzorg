<?php

$newsletter_title = get_field('newsletter_title', 'option');
$newsletter_text = get_field('newsletter_text', 'option');

?>

<div class="section m-newsletter padding-resp-top padding-resp-bottom bg--color-primary">

    <div class="l-container">
        <div class="l-row">
            <div class="col-xs-12">


                <!-- Begin MailChimp Signup Form -->
                <!-- <link href="http://cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css"> -->
                <!-- <style type="text/css">
                	#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                </style> -->
                <div id="mc_embed_signup">
                <form action="http://kernelementenouderenzorg.us16.list-manage.com/subscribe/post?u=63ec45f1dde0e559f12228b6e&amp;id=ef8b935b7c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">

                    <div class="m-newsletter__text">
                    	<h2 class="txt--white"><?php echo $newsletter_title; ?></h2>
                        <p class="txt--white"><?php echo $newsletter_text; ?></p>
                    </div>

                <div class="mc-field-group">
                	<label for="mce-EMAIL">E-mailadres  <span class="asterisk">*</span>
                </label>
                	<input placeholder="E-mailadres..." type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                </div>
                <div class="mc-field-group">
                	<label for="mce-VOORNAAM">Voornaam </label>
                	<input placeholder="Voornaam..." type="text" value="" name="VOORNAAM" class="" id="mce-VOORNAAM">
                </div>

                <div class="mc-field-group">
                	<label for="mce-ACHTERNAAM">Achternaam </label>
                	<input placeholder="Achternaam..." type="text" value="" name="ACHTERNAAM" class="" id="mce-ACHTERNAAM">
                </div>
                	<div id="mce-responses" class="clear">
                		<div class="response" id="mce-error-response" style="display:none"></div>
                		<div class="response" id="mce-success-response" style="display:none"></div>
                	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_63ec45f1dde0e559f12228b6e_ef8b935b7c" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Inschrijven" name="subscribe" id="mc-embedded-subscribe" class="button o-btn o-btn--primary"></div>
                    </div>
                </form>
                </div>
                <script type='text/javascript' src='http://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='VOORNAAM';ftypes[1]='text';fnames[4]='TUSSENV';ftypes[4]='text';fnames[2]='ACHTERNAAM';ftypes[2]='text'; /*
                 * Translated default messages for the $ validation plugin.
                 * Locale: NL
                 */
                $.extend($.validator.messages, {
                        required: "Dit is een verplicht veld.",
                        remote: "Controleer dit veld.",
                        email: "Vul hier een geldig e-mailadres in.",
                        url: "Vul hier een geldige URL in.",
                        date: "Vul hier een geldige datum in.",
                        dateISO: "Vul hier een geldige datum in (ISO-formaat).",
                        number: "Vul hier een geldig getal in.",
                        digits: "Vul hier alleen getallen in.",
                        creditcard: "Vul hier een geldig creditcardnummer in.",
                        equalTo: "Vul hier dezelfde waarde in.",
                        accept: "Vul hier een waarde in met een geldige extensie.",
                        maxlength: $.validator.format("Vul hier maximaal {0} tekens in."),
                        minlength: $.validator.format("Vul hier minimaal {0} tekens in."),
                        rangelength: $.validator.format("Vul hier een waarde in van minimaal {0} en maximaal {1} tekens."),
                        range: $.validator.format("Vul hier een waarde in van minimaal {0} en maximaal {1}."),
                        max: $.validator.format("Vul hier een waarde in kleiner dan of gelijk aan {0}."),
                        min: $.validator.format("Vul hier een waarde in groter dan of gelijk aan {0}.")
                });}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                <!--End mc_embed_signup-->

            </div>
        </div>
    </div>
</div>