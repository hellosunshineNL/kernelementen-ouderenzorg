<?php

$facebook_aanwezig = get_field('facebook_aanwezig', 'option');
$twitter_aanwezig = get_field('twitter_aanwezig', 'option');
$google_plus_aanwezig = get_field('google_plus_aanwezig', 'option');
$linkedin_aanwezig = get_field('linkedin_aanwezig', 'option');
$facebook_url = get_field('facebook_url', 'option');
$twitter_url = get_field('twitter_url', 'option');
$google_plus_url = get_field('google_plus_url', 'option');
$linkedin_url = get_field('linkedin_url', 'option');

?>

<div class="m-social-media">

	<?php if ($facebook_aanwezig) { ?>
		<a href="<?php echo($facebook_url); ?>" target="_blank" class="m-social-media__button"><i class="fa fa-facebook"></i></a>			
	<?php } ?>

	<?php if ($twitter_aanwezig) { ?>
		<a href="<?php echo($twitter_url); ?>" target="_blank" class="m-social-media__button"><i class="fa fa-twitter"></i></a>			
	<?php } ?>

	<?php if ($google_plus_aanwezig) { ?>
		<a href="<?php echo($google_plus_url); ?>" target="_blank" class="m-social-media__button"><i class="fa fa-google-plus"></i></a>			
	<?php } ?>

	<?php if ($linkedin_aanwezig) { ?>
		<a href="<?php echo($linkedin_url); ?>" target="_blank" class="m-social-media__button"><i class="fa fa-linkedin"></i></a>			
	<?php } ?>
	
</div>