<div class="m-copyright-bar" role="copyright footer">
	<div class="l-container">
		<div class="m-copyright-bar__text"><p>&copy; <?php echo date('Y'); ?> kernelementenouderenzorg.nl  |  <a href="/disclaimer">Disclaimer</a></p>
		</div>
	</div>
	<div class="o-to-top-btn right">
		<i class="fa fa-angle-up"></i>
	</div>
</div>