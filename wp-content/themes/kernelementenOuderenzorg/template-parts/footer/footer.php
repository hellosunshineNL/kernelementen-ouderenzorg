<?php

$footer_contact = get_field('footer_contact', 'option');
$footer_tekst = get_field('footer_text_middle', 'option');

?>

<footer id="l-footer" class="m-footer">

	<div class="l-container padding-resp-top padding-resp-bottom">
		<div class="l-row">
			<div class="m-footer__col col-l col-xs-12 col-sm-4">
				<h3>Links</h3>
				
				<?php wp_nav_menu( array( 'menu' => 'main-menu')); ?>
				
			</div>
			<div class="m-footer__col col-c col-xs-12 col-sm-4">
				
				<?php get_template_part('template-parts/social-media/social-media'); ?>

				<!--<div class="m-footer__divider"></div>-->
				<?php // echo $footer_tekst; ?>

			</div>
			<div class="m-footer__col col-r col-xs-12 col-sm-4">
				
				<h3>Contact</h3>
				<?php echo $footer_contact; ?>

			</div>

			<div class="m-footer__col-mobile col-xs-12 ">
				
				<?php get_template_part('template-parts/social-media/social-media'); ?>
				<br><br>
				<?php echo $footer_tekst; ?>

			</div>

		</div>
	</div>
	
	<!-- Copyright bar -->
	<?php get_template_part('template-parts/footer/copyright'); ?>

<footer>