<nav class="m-navigation">
	<div class="m-navigation__mobile-toggle">

		<button class="hamburger hamburger--squeeze m-navigation__mobile-toggle__btn" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
			<!-- <span class="hamburger__label">Menu</span> -->
		</button>
	</div>
	<div class="m-navigation__container">
		<?php wp_nav_menu( array( 'menu' => 'main-menu')); ?>
	</div>
</nav>