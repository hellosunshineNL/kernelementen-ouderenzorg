<?php

$questions_1_titel = get_field('questions_1_titel');
$questions_1_tekst = get_field('questions_1_tekst');
$questions_1_image = get_field('questions_1_image');


?>

<div class="section m-questions m-questions--model bg--grayF4 padding-resp-top padding-resp-bottom">
	<div class="l-container">

		<div class="l-row">
			<div class="col-xs-12 col-sm-4">
				<h2><?php echo $questions_1_titel; ?></h2>
				<?php echo $questions_1_tekst; ?>
			</div>
			<div class="col-xs-12 col-sm-8 negative-top">
				
				<div class="m-questions__image" style="background: url(<?php echo $questions_1_image; ?>) no-repeat center center; background-size: cover;">
				</div>
				<div class="m-questions__speech">
					<div class="m-questions__speech__content bg--white padding-resp">
						<ul>
						
						<?php

							// check if the repeater field has rows of data
							if( have_rows('questions_1_vragen') ):
								
							 	// loop through the rows of data
							    while ( have_rows('questions_1_vragen') ) : the_row();

							        // display a sub field value
									$questions_1_vraag = get_sub_field('questions_1_vraag');
							        
							        ?>
										
										<li><?php echo($questions_1_vraag); ?></li>

							        <?php
						        
							    endwhile;

							else :

							    // no rows found

							endif;

						?>

						</ul>

					</div>
					<div class="m-questions__speech__triangle">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>