<?php

$questions_2_titel = get_field('questions_2_titel');
$questions_2_tekst = get_field('questions_2_tekst');
$questions_2_image = get_field('questions_2_image');

?>

<div class="section m-questions m-questions--right bg--grayF4 padding-resp-top padding-resp-bottom--xl">
	<div class="l-container">
		<div class="l-row">

			<div class="col-xs-12 col-sm-8 m-questions__col-image">

				<h2><?php echo $questions_2_titel; ?></h2>
				<div class="m-questions__image" style="background: url(<?php echo $questions_2_image; ?>) no-repeat center center; background-size: cover;">
				</div>

			</div>
			<div class="col-xs-12 col-sm-4 m-questions__col-text">
				
				<div class="m-questions__speech">
					<div class="m-questions__speech__content bg--white padding-resp">
						<ul>
							
							<?php

							// check if the repeater field has rows of data
							if( have_rows('questions_2_vragen') ):
								
							 	// loop through the rows of data
							    while ( have_rows('questions_2_vragen') ) : the_row();

							        // display a sub field value
									$questions_2_vraag = get_sub_field('questions_2_vraag');
							        
							        ?>
										
										<li><?php echo($questions_2_vraag); ?></li>

							        <?php
						        
							    endwhile;

							else :

							    // no rows found

							endif;

						?>

						</ul>
					</div>
					<div class="m-questions__speech__triangle">

					</div>
				</div>
				
			</div>

		</div>
	</div>
</div>