<?php

$questions_3_titel = get_field('questions_3_titel');
$questions_3_tekst = get_field('questions_3_tekst');
$questions_3_image = get_field('questions_3_image');

?>

<div class="section m-questions m-questions--left bg--white padding-resp-top padding-resp-bottom">
	<div class="l-container">
		<div class="l-row">
			
			<div class="col-xs-12 col-sm-8 m-questions__col-image m-questions__col-mobile">
				
				<h2><?php echo $questions_3_titel; ?></h2>
				<div class="m-questions__image" style="background: url(<?php echo $questions_3_image; ?>) no-repeat center center; background-size: cover;">
				</div>

			</div>

			<div class="col-xs-12 col-sm-4 m-questions__col-text">

				<div class="m-questions__speech">
					<div class="m-questions__speech__content bg--white padding-resp">
						<ul>
							
							<?php

							// check if the repeater field has rows of data
							if( have_rows('questions_3_vragen') ):
								
							 	// loop through the rows of data
							    while ( have_rows('questions_3_vragen') ) : the_row();

							        // display a sub field value
									$questions_3_vraag = get_sub_field('questions_3_vraag');
							        
							        ?>
										
										<li><?php echo($questions_3_vraag); ?></li>

							        <?php
						        
							    endwhile;

							else :

							    // no rows found

							endif;

						?>

						</ul>
					</div>
					<div class="m-questions__speech__triangle">

					</div>
				</div>
				
			</div>
			<div class="col-xs-12 col-sm-8 m-questions__col-image m-questions__col-desktop negative-top">
				
				<div class="m-questions__image" style="background: url(<?php echo $questions_3_image; ?>) no-repeat center center; background-size: cover;">
				</div>
				<h2><?php echo $questions_3_titel; ?></h2>

			</div>

		</div>
	</div>
</div>