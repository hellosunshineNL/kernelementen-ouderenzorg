<?php

$voordelen_titel = get_field('voordelen_titel');

?>

<div class="section m-voordelen bg--gray4 padding-resp-top padding-resp-bottom">
	<div class="l-container">
		<div class="l-row">

			<div class="col-xs-12">
				<h2 class="txt--white"><?php echo $questions_1_titel; ?></h2>
				<ul>

					<?php

							// check if the repeater field has rows of data
							if( have_rows('voordelen_items') ):
								
							 	// loop through the rows of data
							    while ( have_rows('voordelen_items') ) : the_row();

							        // display a sub field value
									$voordelen_item = get_sub_field('voordelen_item');
							        
							        ?>
										
										<li><i class="fa fa-check"></i><?php echo($voordelen_item); ?></li>

							        <?php
							        
							    endwhile;

							else :

							    // no rows found

							endif;

						?>
				</ul>
			</div>
		</div>
	</div>
</div>