<?php get_header(); ?>

	<div id="l-wrapper">
		
		<?php get_template_part('template-parts/header/header'); ?>

				<?php 
						// Start the loop. 
						while ( have_posts() ) : the_post();  ?>

								<!-- <div id="l-content">
									<div class="l-container">
										<div class="m-content">
											
											<?php  
												// the_post();
						      // 					the_content();
											?>
										</div>
									</div>
								</div> -->
								<?php the_field('sub_heading'); ?>
								<?php get_template_part('template-parts/intro/intro-home'); ?>
								<?php get_template_part('template-parts/kernelementen/kernelementen'); ?>
								<?php get_template_part('template-parts/attention/attention'); ?>
								<?php get_template_part('template-parts/footer/footer'); ?>

						      <?php
					    
					    // End of the loop.
						endwhile;
					?>	
				

	</div>

<?php get_footer(); ?>