
<?php get_header(); ?>

	<div id="l-wrapper">
		
		<?php get_template_part('template-parts/header/header'); ?>

				<?php 
						// Start the loop. 
						while ( have_posts() ) : the_post();  ?>

							<?php get_template_part('template-parts/intro/intro-volgpagina'); ?>

							<div class="section bg--white padding-resp-top padding-resp-bottom">
								<div class="l-container">
									<div class="l-row">

										<div class="col-xs-12">
											<div class="m-content">
												<?php the_content(); ?>
											</div>
										</div>
									</div>
								</div>
							</div>

						<?php
					    
					    // End of the loop.
						endwhile;
					?>	
			
			<?php get_template_part('template-parts/footer/footer'); ?>

	</div>

<?php get_footer(); ?>