 <?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

	<div id="l-wrapper">
		
		<?php get_template_part('template-parts/header/header'); ?>

				<?php 
					// Start the loop. 
					while ( have_posts() ) : the_post();  ?>

							<?php get_template_part('template-parts/intro/intro-home'); ?>
							<?php get_template_part('template-parts/kernelementen/kernelementen'); ?>
							<?php get_template_part('template-parts/attention/attention'); ?>
							<?php get_template_part('template-parts/newsletter-subscribe/newsletter-subscribe'); ?>

					      <?php
				    
				    // End of the loop.
					endwhile;
				?>	
				
				<?php get_template_part('template-parts/footer/footer'); ?>

	</div>

<?php get_footer(); ?>