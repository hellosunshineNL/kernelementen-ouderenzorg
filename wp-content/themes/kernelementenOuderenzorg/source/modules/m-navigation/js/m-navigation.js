jQuery.extend({

	mNavigation: function(holder){

		// PUBLIC
		var _this               = this;

		var _private = {

			// PRIVATE OBJECTS
			holder  : _this,
			mobileButton    : null,
			navOverlay  : null,


			// PRIVATE FUNCTIONS
			setup:function()
			{
				_private.holder             = holder;
				_private.mobileButton       = _private.holder.find('.m-navigation__mobile-toggle__btn');
				_private.navOverlay         = _private.holder.find('.m-navigation__container');

			},
			clickListener:function() {
				_private.mobileButton.bind('click', function() {
					_private.navOverlay.toggleClass('is--active');
					$(this).toggleClass('is-active');
				});

			},


			resetNav:function() {
				// _private.navOverlay.removeClass('is--active');
				// _private.mobileButton.find('i').removeClass('fa-times').addClass('fa-bars');
				// _private.hasChildrenLink.removeClass('is--active');
				// _private.holder.css({'overflow': 'visible', 'height': 'auto'});
				// _private.navOverlay.find('.sub-menu').slideUp();
			}

		};
		// INITIALIZE APPLICATION
		if($(holder).length > 0) {
			_private.setup();
			_private.clickListener();

		}
			// WINDOW RESIZE
		function doResize() {

			_private.resetNav();
		}
		var resizeTimer;
		$(window).resize(function() {
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(doResize, 100);
		});
	}
});
$(function()
{
	$(document).ready(function()
	{
		var mNavigation = new $.mNavigation($(".m-navigation"));
	});
});
