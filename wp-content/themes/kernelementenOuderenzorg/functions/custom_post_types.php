<?php

	define("BASE_CPT", __dir__."/cpt/");
	
	$filenames = preg_grep('~\.(php)$~', scandir(BASE_CPT));
	
	foreach ($filenames as $filename) {
		include(BASE_CPT.$filename);
	}

?>
