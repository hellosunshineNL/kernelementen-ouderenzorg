<?php

	if( function_exists('acf_add_local_field_group') ):
	
		define("BASE_ACF", __dir__."/acf/");
		
		$filenames =preg_grep('~\.(php)$~', scandir(BASE_ACF));
		
		foreach ($filenames as $filename) {
			include(BASE_ACF.$filename);
		}

	endif;

?>
