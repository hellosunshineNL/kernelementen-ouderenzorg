 <?php
/*
Template Name: Opleiding
*/
?>

<?php get_header(); ?>

	<div id="l-wrapper">
		
		<?php get_template_part('template-parts/header/header'); ?>

				<?php 
						// Start the loop. 
						while ( have_posts() ) : the_post();  ?>

								<?php get_template_part('template-parts/intro/intro-volgpagina'); ?>
								<?php get_template_part('template-parts/voordelen/voordelen');?>
								<?php get_template_part('template-parts/questions/questions-right');?>
								<?php get_template_part('template-parts/questions/questions-left');?>

						      <?php
					    
					    // End of the loop.
						endwhile;
					?>	
			
			<?php get_template_part('template-parts/footer/footer'); ?>

	</div>

<?php get_footer(); ?>