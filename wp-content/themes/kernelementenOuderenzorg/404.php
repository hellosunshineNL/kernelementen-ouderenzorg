
<?php get_header(); ?>

	<div id="l-wrapper">
		
		<?php get_template_part('template-parts/header/header'); ?>

				
			<div class="section m-intro-volgpagina bg--color-primary padding-resp-top padding-resp-bottom">

				<div class="l-container">
					<div class="l-row">

						<div class="col-xs-12">
							<div class="m-intro-volgpagina__content">
								<h1>Oeps! Sorry, we kunnen deze pagina niet meer vinden</h1>
								
							</div>
						</div>

					</div>
				</div>

			</div>

			<div class="section bg--white padding-resp-top padding-resp-bottom">
				<div class="l-container">
					<div class="l-row">

						<div class="col-xs-12">
							<div class="m-content">
								<p>Het lijkt erop dat deze pagina niet (meer) bestaat of misschien verhuisd is.</p>
								<br><br>
								<a href="/">Terug naar home</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		<?php get_template_part('template-parts/footer/footer'); ?>

	</div>

<?php get_footer(); ?>