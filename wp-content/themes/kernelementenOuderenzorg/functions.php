<?php

	// includes custom post types from CPT plugin
	//require_once('functions/custom_post_types.php');

	// includes custom post types from ACF plugin
	// require_once('functions/custom_fields.php');


	//ADD MENU SUPPPORT IN THE THEME
	function register_my_menus() {
	  register_nav_menus(
	    array(
	    'main-menu' => __( 'Main menu' )
	    ));
	}
	add_action( 'init', 'register_my_menus' );


	// Register widget areas
	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name'=> 'Footer widget 1',
			'id' => 'footer-widget-1',
			'class' => 'footer__widget__left',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="txt--gray">',
			'after_title' => '</h5>',
		));
		register_sidebar(array(
			'name'=> 'Footer widget 2',
			'id' => 'footer-widget-2',
			'class' => 'footer__widget__mid-left',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="txt--gray">',
			'after_title' => '</h5>',
		));
		register_sidebar(array(
			'name'=> 'Footer widget 3',
			'id' => 'footer-widget-3',
			'class' => 'footer__widget__mid-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="txt--gray">',
			'after_title' => '</h5>',
		));
		register_sidebar(array(
			'name'=> 'Footer widget 4',
			'id' => 'footer-widget-4',
			'class' => 'footer__widget__right',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="txt--gray">',
			'after_title' => '</h5>',
		));

	}

	function remove_meta_boxes() {
		// meta box form pages
		remove_meta_box( 'categorydiv', 'page', 'normal' );
		remove_meta_box( 'tagsdiv-post_tag', 'page', 'normal' );
		remove_meta_box( 'commentsdiv', 'page', 'normal' );
		remove_meta_box( 'commentstatusdiv', 'page', 'normal' );
		remove_meta_box( 'slugdiv', 'page', 'normal' );
		remove_meta_box( 'postcustom', 'page', 'normal' );
		remove_meta_box( 'postimagediv', 'page', 'normal' );

		// meta box form posts
		remove_meta_box( 'categorydiv', 'post', 'normal' );
		remove_meta_box( 'tagsdiv-post_tag', 'post', 'normal' );
		remove_meta_box( 'commentsdiv', 'post', 'normal' );
		remove_meta_box( 'commentstatusdiv', 'post', 'normal' );
		remove_meta_box( 'slugdiv', 'post', 'normal' );
		remove_meta_box( 'postcustom', 'post', 'normal' );
		remove_meta_box( 'postimagediv', 'post', 'normal' );

		// meta box form specialismen
		remove_meta_box( 'categorydiv', 'specialismen', 'normal' );
		remove_meta_box( 'tagsdiv-post_tag', 'specialismen', 'normal' );
		remove_meta_box( 'commentsdiv', 'specialismen', 'normal' );
		remove_meta_box( 'commentstatusdiv', 'specialismen', 'normal' );
		remove_meta_box( 'slugdiv', 'specialismen', 'normal' );
		remove_meta_box( 'postcustom', 'specialismen', 'normal' );
		remove_meta_box( 'postimagediv', 'specialismen', 'normal' );

		// meta box form medewerkers
		remove_meta_box( 'categorydiv', 'medewerkers', 'normal' );
		remove_meta_box( 'tagsdiv-post_tag', 'medewerkers', 'normal' );
		remove_meta_box( 'commentsdiv', 'medewerkers', 'normal' );
		remove_meta_box( 'commentstatusdiv', 'medewerkers', 'normal' );
		remove_meta_box( 'slugdiv', 'medewerkers', 'normal' );
		remove_meta_box( 'postcustom', 'medewerkers', 'normal' );
		remove_meta_box( 'postimagediv', 'medewerkers', 'normal' );
	}
	add_action( 'admin_menu', 'remove_meta_boxes' );

	// Remove posts and comments from admin menu
	function remove_posts_menu() {
	    remove_menu_page('edit.php');
			remove_menu_page('edit-comments.php');
	}
	add_action('admin_init', 'remove_posts_menu');

	// Remove comments from top bar
	function my_admin_bar_render() {
	    global $wp_admin_bar;
	    $wp_admin_bar->remove_menu('comments');
	}
	add_action( 'wp_before_admin_bar_render', 'my_admin_bar_render' );



	function clean_head () {
	    remove_action('wp_head', 'wp_generator');                // #1
	    remove_action('wp_head', 'wlwmanifest_link');            // #2
	    remove_action('wp_head', 'rsd_link');                    // #3
	    remove_action('wp_head', 'wp_shortlink_wp_head');        // #4

	    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);    // #5

	    add_filter('the_generator', '__return_false');            // #6
	    add_filter('show_admin_bar','__return_false');            // #7

	    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );  // #8
	    remove_action( 'wp_print_styles', 'print_emoji_styles' );
	}

	add_action('after_setup_theme', 'clean_head');

?>